#!/usr/bin/env python

def main():
  import argparse

  parser = argparse.ArgumentParser(description="Plot V(Out) of one or more SPICE simulation raw files")
  parser.add_argument('-j', '--jsonFile',
                      required=True,
                      help="JSON file describing which raw files to use")
  parser.add_argument('-i', '--plotInput',
                      action="store_true",
                      help="Set this option if you want to draw the input current in addition to the output voltages")

  args = parser.parse_args()

  import os
  import json

  with open(args.jsonFile) as jsonFile:
    import ltspice
    import matplotlib.pyplot as plt
    import numpy as np

    jsonData = json.load(jsonFile)

    ax1 = plt.subplot()
    ax2 = None

    lines = None
    curLine = None

    plotData = jsonData["data"]
    for data in plotData:
      data["ltspice"] = ltspice.Ltspice(data["data"])
      try:
        data["ltspice"].parse()
      except ltspice.FileSizeNotMatchException:
        data["ltspice"].set_variable_dtype(float)
        data["ltspice"].parse()

      data["time"] = data["ltspice"].get_time()
      data["VOut"] = data["ltspice"].get_data('v(/out)')

      data["line"] = ax1.plot(data["time"], data["VOut"], label=data["name"])

      if lines is None:
        lines = data["line"]
      else:
        lines += data["line"]

      if ax2 is None and args.plotInput:
        ax2 = ax1.twinx()
        data["IIn"] = data["ltspice"].get_data('i(@rsim2[i])')

        data["current line"] = ax2.plot(data["time"], -data["IIn"], '-r', label="Input Current")
        curLine = data["current line"]

    if curLine is not None:
      lines += curLine
    labs = [l.get_label() for l in lines]
    plt.legend(lines, labs)

    #ax1.legend()
    ax1.set_xlabel("time [s]")
    ax1.set_ylabel(r"V$_\mathrm{Out}$ [V]")
    if ax2 is not None:
      #ax2.legend()
      ax2.set_ylabel(r"-I$_\mathrm{In}$ [A]")

    plt.tight_layout()
    plt.show()


def holder():
  import ltspice
  import matplotlib.pyplot as plt
  import numpy as np
  import os

  l = ltspice.Ltspice(os.path.dirname(__file__)+'\\rc.raw') 
  # Make sure that the .raw file is located in the correct path
  l.parse() 

  time = l.get_time()
  V_source = l.get_data('V(source)')
  V_cap = l.get_data('V(cap)')

  plt.plot(time, V_source)
  plt.plot(time, V_cap)
  plt.show()


if __name__ == "__main__":
  main()

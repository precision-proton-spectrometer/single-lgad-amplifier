# Single LGAD Amplifier

A KiCAD schematic of an amplifier for a single channel from an LGAD sensor.
The schematic has the goal af allowing to run the ngspice simulation tool but also
as a support for a board design.

The diagram is effectively a copy from the
[Chubut Board](https://github.com/SengerM/ChubutBoard) with some updates.
The Chubut board is in turn based on the
[UCSC "Santa Cruz" Board](https://twiki.cern.ch/twiki/bin/view/Main/UcscSingleChannel).

## Notes
The repository does not include the SPICE model for the transistor by default.
This can be obtained from the Infineon website from
[link](https://www.infineon.com/dgdl/Infineon-RFTransistor-SPICE.lib-SM-v02_10-EN.lib?fileId=5546d46256fb43b30157577dffb04549)
and saved in the root directory as `BFP840FESD.lib`.

## Running scripts
Create a venv for running the python scripts: `python3 -m venv venv`

Install the packages from the requirements file: `python -m pip install requirements.txt`

Source the activate script to activate the venv and remember to always do that when working on stuff

Save the requirements for others to be able to create the same venv: `python -m pip freeze > requirements.txt`
